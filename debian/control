Source: clonalframeml
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/clonalframeml
Vcs-Git: https://salsa.debian.org/med-team/clonalframeml.git
Homepage: https://github.com/xavierdidelot/ClonalFrameML
Rules-Requires-Root: no

Package: clonalframeml
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Efficient Inference of Recombination in Whole Bacterial Genomes
 ClonalFrameML is a software package that performs efficient inference of
 recombination in bacterial genomes. ClonalFrameML was created by Xavier
 Didelot and Daniel Wilson. ClonalFrameML can be applied to any type of
 aligned sequence data, but is especially aimed at analysis of whole
 genome sequences. It is able to compare hundreds of whole genomes in a
 matter of hours on a standard Desktop computer. There are three main
 outputs from a run of ClonalFrameML: a phylogeny with branch lengths
 corrected to account for recombination, an estimation of the key
 parameters of the recombination process, and a genomic map of where
 recombination took place for each branch of the phylogeny.
 .
 ClonalFrameML is a maximum likelihood implementation of the Bayesian
 software ClonalFrame which was previously described by Didelot and
 Falush (2007). The recombination model underpinning ClonalFrameML is
 exactly the same as for ClonalFrame, but this new implementation is a
 lot faster, is able to deal with much larger genomic dataset, and does
 not suffer from MCMC convergence issues
